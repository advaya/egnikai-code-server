#!/usr/bin/env bash
set -e
s3FileToDownload=$1
fileName=$(basename $s3FileToDownload)
aws s3 cp ${s3FileToDownload} . > /dev/null
tar -xzf ${fileName} -C .
mkdir classes
javac -nowarn -d classes $(find src/main/java/ -name "*.java")
javac -nowarn -d classes -cp "classes/:./junitlibs/*" $(find src/test/java -name "*.java")
testFilenames=$(node findTestClassNames.js classes)
java -cp "classes/:./junitlibs/*" org.junit.runner.JUnitCore ${testFilenames}