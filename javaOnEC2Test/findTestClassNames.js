const baseDirectory = process.argv[2];
const fs=require('fs');
const testFileNames = [];

function readDirectory(testFileNames, directory, baseDirectory) {
  const files = fs.readdirSync(directory);
  for(let file of files) {
    if (fs.statSync(`${directory}/${file}`).isDirectory()) {
      readDirectory(testFileNames, `${directory}/${file}`, baseDirectory);
    } else {
      if(file.endsWith("Test.class")) {
        testFileNames.push(`${directory.replace(baseDirectory+'/', '').replace('/','.')}.${file.replace('.class','')}`);
      }
    }
  }
}

readDirectory(testFileNames, baseDirectory, baseDirectory);

console.log(testFileNames.join(' '));
