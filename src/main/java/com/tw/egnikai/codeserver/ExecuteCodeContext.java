package com.tw.egnikai.codeserver;

public class ExecuteCodeContext {

    private String s3FileName;

    public String getS3FileName() {
        return s3FileName;
    }

    public void setS3FileName(String s3FileName) {
        this.s3FileName = s3FileName;
    }

    @Override
    public String toString() {
        return "ExecuteCodeContext{" +
                "s3FileName='" + s3FileName + '\'' +
                '}';
    }
}
