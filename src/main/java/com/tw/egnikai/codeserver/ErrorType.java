package com.tw.egnikai.codeserver;

public enum ErrorType {
        COMPILATION_FAILED,
        TESTS_FAILED
}
