package com.tw.egnikai.codeserver;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;

@SpringBootApplication
@RestController
public class CodeServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeServerApplication.class, args);
    }

    @Value("${access-key-id:''}")
    private String accessKeyId;
    @Value("${secret-key-id:''}")
    private String secretKeyId;
    @Value("${region:''}")
    private String region;


    @PostMapping("/execute")
    @ResponseBody
    public OutputMessage executeCode(@RequestBody ExecuteCodeContext context)  {
        System.out.println("execute called for "+ context);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            CommandLine commandline = CommandLine.parse(String.format("docker run --rm " +
                    "-e AWS_ACCESS_KEY_ID=%s " +
                    "-e AWS_SECRET_ACCESS_KEY=%s " +
                    "-e AWS_DEFAULT_REGION=%s " +
                    "regsethu/java-on-ec2-test %s", accessKeyId, secretKeyId, region, context.getS3FileName()));
            DefaultExecutor exec = new DefaultExecutor();

            PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
            exec.setStreamHandler(streamHandler);
            exec.execute(commandline);
            return new OutputMessage(true,null, null);
        } catch (Exception e) {
            JavaErrorParser errorParser = new JavaErrorParser();
            return errorParser.parseError(outputStream.toString());
        }
    }
}
