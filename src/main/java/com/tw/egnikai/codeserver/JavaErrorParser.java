package com.tw.egnikai.codeserver;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class JavaErrorParser {

    private static String TEST_FAILED_REGEX = "\\d+\\) (.*)";

    public OutputMessage parseError(String errorString) {
        String[] lines = errorString.split("\n");
        if(lines[0].contains("JUnit version 4.12")) {
            return parseTestError(lines);
        } else {
            return parseCompilationError(errorString);
        }
    }

    private OutputMessage parseCompilationError(String errorString) {
        return new OutputMessage(false,ErrorType.COMPILATION_FAILED, errorString);
    }

    private OutputMessage parseTestError(String[] lines) {
        StringBuilder message = new StringBuilder("There were test failures");
        Pattern pattern = Pattern.compile(TEST_FAILED_REGEX);

        for(String line: lines) {
            Matcher matcher = pattern.matcher(line);
            message.append(matcher.find() && !matcher.group(1).contains("hidden") ? "\n"+matcher.group(1) : "");
        }

        return new OutputMessage(false,ErrorType.TESTS_FAILED, message.toString());
    }
}
