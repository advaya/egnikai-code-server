package com.tw.egnikai.codeserver;

public class OutputMessage {
    private boolean isSuccess;
    private ErrorType errorType;
    private String message;

    public OutputMessage(boolean isSuccess, ErrorType errorType, String message) {
        this.isSuccess = isSuccess;
        this.errorType = errorType;
        this.message = message;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public String getMessage() {
        return message;
    }

}
